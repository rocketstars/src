import tkinter as tk
from tkinter import font,IntVar,StringVar
import RPi.GPIO as GPIO
import time
import serial
import os, sys


GPIO.setwarnings(False)

root = tk.Tk()
root.title("EEE174 - Project")
#counting
#counter = None
#counter = tk.IntVar()
#counter.set(0)
#widget(sensor data)
#data_w1 = None


#*****************************************************************
readpin = 5
readpinChar = None
#MUXpins
s0=6
s1=13
s2=19
s3=26
# ****************************************************************
GPIO.setmode(GPIO.BCM)
GPIO.setup(readpin,GPIO.IN)
GPIO.setup(s0, GPIO.OUT)
GPIO.setup(s1, GPIO.OUT)
GPIO.setup(s2, GPIO.OUT)
GPIO.setup(s3, GPIO.OUT)
# Initial state for LED:
GPIO.output(s0, GPIO.LOW)
GPIO.output(s1, GPIO.LOW)
GPIO.output(s2, GPIO.LOW)
GPIO.output(s3, GPIO.LOW)
#
print_w1 = StringVar()
print_w2 = StringVar()
print_w3 = StringVar()
print_w4 = StringVar()
print_w5 = StringVar()
print_w6 = StringVar()
print_w7 = StringVar()
print_w8 = StringVar()
print_w9 = StringVar()
print_w10 = StringVar()
#
root.w1 = IntVar()
root.w2 = IntVar()
root.w3 = IntVar()
root.w4 = IntVar()
root.w5 = IntVar()
root.w6 = IntVar()
root.w7 = IntVar()
root.w8 = IntVar()
root.w9 = IntVar()
root.w10 = IntVar()
#
stoploop = None
#FUNCTIONS*********************************************
#multiplexer*******************************************
def turnon(x):
    if x == 0: 
        GPIO.output(s0, GPIO.LOW)# 0000
        GPIO.output(s1, GPIO.LOW)#
        GPIO.output(s2, GPIO.LOW)#
        GPIO.output(s3, GPIO.LOW)#
    if x == 1: 
        GPIO.output(s0, GPIO.HIGH)#0001 
        GPIO.output(s1, GPIO.LOW)#
        GPIO.output(s2, GPIO.LOW)#
        GPIO.output(s3, GPIO.LOW)#
    if x == 2: 
        GPIO.output(s0, GPIO.LOW)#0010
        GPIO.output(s1, GPIO.HIGH)#
        GPIO.output(s2, GPIO.LOW)#
        GPIO.output(s3, GPIO.LOW)#
    if x == 3: 
        GPIO.output(s0, GPIO.HIGH)#0011 
        GPIO.output(s1, GPIO.HIGH)#
        GPIO.output(s2, GPIO.LOW)#
        GPIO.output(s3, GPIO.LOW)#
    if x == 4: 
        GPIO.output(s0, GPIO.LOW)#0100
        GPIO.output(s1, GPIO.LOW)#
        GPIO.output(s2, GPIO.HIGH)#
        GPIO.output(s3, GPIO.LOW)#
    if x == 5: 
        GPIO.output(s0, GPIO.HIGH)#0101
        GPIO.output(s1, GPIO.LOW)#
        GPIO.output(s2, GPIO.HIGH)#
        GPIO.output(s3, GPIO.LOW)#
    if x == 6: 
        GPIO.output(s0, GPIO.LOW)#0110
        GPIO.output(s1, GPIO.HIGH)#
        GPIO.output(s2, GPIO.HIGH)#
        GPIO.output(s3, GPIO.LOW)#
    if x == 7: 
        GPIO.output(s0, GPIO.HIGH)#0111
        GPIO.output(s1, GPIO.HIGH)
        GPIO.output(s2, GPIO.HIGH)
        GPIO.output(s3, GPIO.LOW)
    if x == 8: 
        GPIO.output(s0, GPIO.LOW)#1000
        GPIO.output(s1, GPIO.LOW)#
        GPIO.output(s2, GPIO.LOW)#
        GPIO.output(s3, GPIO.HIGH)#
    if x == 9: 
        GPIO.output(s0, GPIO.HIGH)#1001 
        GPIO.output(s1, GPIO.LOW)#
        GPIO.output(s2, GPIO.LOW)#
        GPIO.output(s3, GPIO.HIGH)#
    if x == 10: 
        GPIO.output(s0, GPIO.LOW)#1010
        GPIO.output(s1, GPIO.HIGH)#
        GPIO.output(s2, GPIO.LOW)#
        GPIO.output(s3, GPIO.HIGH)#
    if x == 11: 
        GPIO.output(s0, GPIO.HIGH)#1011
        GPIO.output(s1, GPIO.HIGH)#
        GPIO.output(s2, GPIO.LOW)#
        GPIO.output(s3, GPIO.HIGH)#
    if x == 12: 
        GPIO.output(s0, GPIO.LOW)#1100
        GPIO.output(s1, GPIO.LOW)#
        GPIO.output(s2, GPIO.HIGH)#
        GPIO.output(s3, GPIO.HIGH)#
    if x == 13: 
        GPIO.output(s0, GPIO.HIGH)#1101
        GPIO.output(s1, GPIO.LOW)#
        GPIO.output(s2, GPIO.HIGH)#
        GPIO.output(s3, GPIO.HIGH)#
    if x == 14: 
        GPIO.output(s0, GPIO.LOW)#1110
        GPIO.output(s1, GPIO.HIGH)#
        GPIO.output(s2, GPIO.HIGH)#
        GPIO.output(s3, GPIO.HIGH)#
    if x == 15: 
        GPIO.output(s0, GPIO.HIGH)#1111
        GPIO.output(s1, GPIO.HIGH)#
        GPIO.output(s2, GPIO.HIGH)#
        GPIO.output(s3, GPIO.HIGH)
def left():
    for x in range(16):
             turnon(x)
             time.sleep(0.05)
def right():
    for x in range(16):
             turnon(15-x)
             time.sleep(0.05)
#************************************************* 
def count():
    global counter
    counter.set(counter.get()+1)
#Button Command Loops   ********************************************  
def b1Command():
    ser.write(b'1')#write to uC (1)
    print('1')
def b2Command():
    ser.write(b'2')#write to uC (2)
    print('2')
def b3Command():
    ser.write(b'3')#write to uC (3)
    print('3')
def b4Command():
    ser.write(b'4')#write to uC (4)
    print('4')
def b5Command():
    ser.write(b'5')#write to uC (5)
    print('5')
def b6Command():
    ser.write(b'6')#write to uC (6)
    print('6')
def restartB():
    python = sys.executable
    os.execl(python, python, * sys.argv)
#def autofeederTime():
#    localtime = time.localtime(time.time())#set localtime to time.struct tuple
#    hour = localtime[3]#set seconds equal to index 5 of localtime tuple
#    hour = localtime[3]
#    minute = localtime[4]
#    second = localtime[5]
#    print(second)
#    if second == 15 and minute == 19 and hour == 21:
#        ser.write(b'1') #writes a character of '1' to the propellor
#        time.sleep(1)
#    ser.write(b'0')
##Serial Code#####################################################################

ser = serial.Serial('/dev/ttyUSB0',115200)
#ser = serial.Serial('/dev/ttyACM0',9600)
                       #listen for the input, exit if nothing received in timeout period
def read_setSerial():  #eventually thread this to run infinitely
                       #convert checks into string then send to Comm
    s = [100]
    s = ser.readline()[:-2]
    #split up the string into substrings; can now assign these to output
    print(s.split(b':'))
    a,b,c,d,e,f,g= s.split(b':')
    print(a,b,c,d,e,f,g)
    if root.w1.get() == 1:  #1
        print_w1.set(a)     
    else:                  
        print_w1.set("Off")
    
    if root.w2.get() == 1:  #2
        print_w2.set(b)
    else:
        print_w2.set("Off")
    
    if root.w3.get() == 1:  #3
        print_w3.set(c)
    else:
        print_w3.set("Off")
    
    if root.w4.get() == 1:  #4
        print_w4.set(d)
    else:
        print_w4.set("Off")
    
    if root.w5.get() == 1:  #5
        print_w5.set(e)
    else:
        print_w5.set("Off")

    if root.w6.get() == 1:  #6
        print_w6.set(f)
    else:
        print_w6.set("Off")
    
    if root.w7.get() == 1:  #7
        print_w7.set(g)
    else:
        print_w7.set("Off")
#    
#    if root.w8.get() == 1:  #8
#        print_w8.set(g)
#    else:
#        print_w8.set("Off")
#    
#    if root.w9.get() == 1:  #9
#        print_w9.set(g)
#    else:
#        print_w9.set("Off")
#    
#    if root.w10.get() == 1: #10
#        print_w10.set(g)
#    else:
#        print_w10.set("Off")
    
    root.after(1000,read_setSerial)

left()
right()
s=[100]
s=ser.readline()
time.sleep(1)
print(s)
print(s)
print(s)
print(s)
print(s)
read_setSerial()
       
    #App
    #buttons##########################################################################
button_font = font.Font(family='Helvetica', size=24, weight='bold')
button1 = tk.Button(root,text="Button1",command=b1Command,font=button_font,bg='light green')
button2 = tk.Button(root,text="Button2",command=b2Command,font=button_font,bg='light green')
button3 = tk.Button(root,text="Button3",command=b3Command,font=button_font,bg='light green')
button4 = tk.Button(root,text="Button4",command=b4Command,font=button_font,bg='light green')
button5 = tk.Button(root,text="Button5",command=b5Command,font=button_font,bg='light green')
button6 = tk.Button(root,text="Button6",command=b6Command,font=button_font,bg='light green')
stopbutton = tk.Button(root,text="   Stop  ",command=root.destroy,font=button_font,bg='red')
restart = tk.Button(root,text=   "Refresh",command=restartB,font=button_font,bg='orange')
button_w1=tk.Checkbutton(root,text='W1',variable=root.w1)
button_w2=tk.Checkbutton(root,text='W2',variable=root.w2)
button_w3=tk.Checkbutton(root,text='W3',variable=root.w3)
button_w4=tk.Checkbutton(root,text='W4',variable=root.w4)
button_w5=tk.Checkbutton(root,text='W5',variable=root.w5)
button_w6=tk.Checkbutton(root,text='W6',variable=root.w6)
button_w7=tk.Checkbutton(root,text='W7',variable=root.w7)
#button_w8=tk.Checkbutton(root,text='W8',variable=root.w8)
#button_w9=tk.Checkbutton(root,text='W9',variable=root.w9)
#button_w10=tk.Checkbutton(root,text='W10',variable=root.w10)
        # Create widgets(outputs)******************************************************************
clock = tk.Label(root, text="Widget 1", borderwidth=8,font=(20))
label_grid_1 = tk.Label(root, text="TOUCH SENSOR", borderwidth=8,font=(20))
label_grid_2 = tk.Label(root, text="WATER LEVEL", borderwidth=8,font=(20))
label_grid_3 = tk.Label(root, text="WATER HEIGHT", borderwidth=8,font=(20))
label_grid_4 = tk.Label(root, text="CLEAN SENSOR", borderwidth=8,font=(20))
label_grid_5 = tk.Label(root, text="TEMPERATURE(F)", borderwidth=8,font=(20))
label_grid_6 = tk.Label(root, text="ALERT(SURR)", borderwidth=8,font=(20))
label_grid_7 = tk.Label(root, text="ALERT(TURT)", borderwidth=8,font=(20))
#label_grid_8 = tk.Label(root, text="Widget 8", borderwidth=8,font=(20))
#label_grid_9 = tk.Label(root, text="Widget 9", borderwidth=8,font=(20))
#label_grid_10 = tk.Label(root, text="Widget 10", borderwidth=8,font=(20))
#
label_grid_space1 = tk.Label(root, text="===", borderwidth=8,font=(20))
label_grid_space2 = tk.Label(root, text="===", borderwidth=8,font=(20))
label_grid_space3 = tk.Label(root, text="===", borderwidth=8,font=(20))
label_grid_space4 = tk.Label(root, text="===", borderwidth=8,font=(20))
label_grid_space5 = tk.Label(root, text="===", borderwidth=8,font=(20))
label_grid_space6 = tk.Label(root, text="===", borderwidth=8,font=(20))
label_grid_space7 = tk.Label(root, text="===", borderwidth=8,font=(20))
#label_grid_space8 = tk.Label(root, text="===", borderwidth=8,font=(20))
#label_grid_space9 = tk.Label(root, text="===", borderwidth=8,font=(20))
#label_grid_space10 = tk.Label(root, text="===", borderwidth=8,font=(20))
#
label_grid_data1 = tk.Label(root, textvariable=print_w1,borderwidth=8,font=(20))
label_grid_data2 = tk.Label(root, textvariable=print_w2,borderwidth=8,font=(20))
label_grid_data3 = tk.Label(root, textvariable=print_w3,borderwidth=8,font=(20))
label_grid_data4 = tk.Label(root, textvariable=print_w4,borderwidth=8,font=(20))
label_grid_data5 = tk.Label(root, textvariable=print_w5,borderwidth=8,font=(20))
label_grid_data6 = tk.Label(root, textvariable=print_w6,borderwidth=8,font=(20))
label_grid_data7 = tk.Label(root, textvariable=print_w7,borderwidth=8,font=(20))
#label_grid_data8 = tk.Label(root, textvariable=print_w8,borderwidth=8,font=(20))
#label_grid_data9 = tk.Label(root, textvariable=print_w9,borderwidth=8,font=(20))
#label_grid_data10 = tk.Label(root, textvariable=print_w10,borderwidth=8,font=(20))

# Lay out widgets in a grid
button1.grid(row=12,column=0)
button2.grid(row=12,column=1)
button3.grid(row=12,column=2)
button4.grid(row=13,column=0)
button5.grid(row=13,column=1)
button6.grid(row=13,column=2)
restart.grid(row=12,column=3)
stopbutton.grid(row=13,column=3)
#
button_w1.grid(row=2, column=3)
button_w2.grid(row=3, column=3)
button_w3.grid(row=4, column=3)
button_w4.grid(row=5, column=3)
button_w5.grid(row=6, column=3)
button_w6.grid(row=7, column=3)
button_w7.grid(row=8, column=3)
#button_w8.grid(row=9, column=3)
#button_w9.grid(row=10, column=3)
#button_w10.grid(row=11, column=3)
    #label_grid_space.grid(row=0, column=0)
    #label_grid_space.grid(row=1, column=0)
label_grid_1.grid(row=2, column=0)
label_grid_2.grid(row=3, column=0)
label_grid_3.grid(row=4, column=0)
label_grid_4.grid(row=5, column=0)
label_grid_5.grid(row=6, column=0)
label_grid_6.grid(row=7, column=0)
label_grid_7.grid(row=8, column=0)
#label_grid_8.grid(row=9, column=0)
#label_grid_9.grid(row=10, column=0)
#label_grid_10.grid(row=11, column=0)
    ###
label_grid_space1.grid(row=2, column=1)
label_grid_space2.grid(row=3, column=1)
label_grid_space3.grid(row=4, column=1)
label_grid_space4.grid(row=5, column=1)
label_grid_space5.grid(row=6, column=1)
label_grid_space6.grid(row=7, column=1)
label_grid_space7.grid(row=8, column=1)
#label_grid_space8.grid(row=9, column=1)
#label_grid_space9.grid(row=10, column=1)
#label_grid_space10.grid(row=11, column=1)
    ###
label_grid_data1.grid(row=2, column=2)
label_grid_data2.grid(row=3, column=2)
label_grid_data3.grid(row=4, column=2)
label_grid_data4.grid(row=5, column=2)
label_grid_data5.grid(row=6, column=2)
label_grid_data6.grid(row=7, column=2)
label_grid_data7.grid(row=8, column=2)
#label_grid_data8.grid(row=9, column=2)
#label_grid_data9.grid(row=10, column=2)
#label_grid_data10.grid(row=11, column=2)
    ###

root.mainloop()    
GPIO.cleanup() 
