#include "simpletools.h"                      // Include simple tools
#include "string.h"
#include <stdio.h>
#include "servo.h"
#include "ping.h"
#include "adcDCpropab.h"

  char touch_out[] = "Default";
  int feedcounter = 0;      //counts number of feeds
  
int main()                                    // Main function
{
  while(1){
      touchsensor();
      levelsensor();
      turbidity();
      heatlamp();
      alertsystem();
      turtleactivity();
      print("\n");
//      autofeeder(); 
 }
}

void touchsensor(void){
 int touch = input(8);         //touch sensor 
 if(touch == 1)
 {
     servo_angle(14, 0);
     pause(1000);
     servo_angle(14, 900);
     pause(1000);
     servo_angle(14, 0);
     servo_stop();
     char touch_out[] = "On";
     print("%s", &touch_out);
    }
  else
  {
      char touch_out[] = "Off";
      print("%s", &touch_out);  
    }      
}  

void levelsensor(void){
 int level;
 int cmDist = ping_cm(0); //Pin 0 turbidity 
 level = 100 - cmDist;  //water level calcs
 if(level < 95)
 {
   print(":Low:%d", level);
   high(1); //turn on pin 1
   pause(1000);   //maybe remove
 } else {
  print(":High:%d", level);
  low(1); 
 }          
} 

void turbidity(void) {
  adc_init(21, 20, 19, 18);
  float v3;
  v3 = adc_volts(3);     //convert A/D Pin 3
  if(v3 < 2.0)
 {
  high(2);
  print(":Dirty Water."); 
 }else {
  low(2);
  print(":Water is OK."); 
 }
}  

void heatlamp(void) {
  adc_init(21, 20, 19, 18);
  float v2;
    v2 = adc_volts(2);        // get voltmeter voltage from A/D input 3
    float temp = (v2 - 0.5) * 100;  //convert input 3 voltage to celsius
    temp = 1.8*temp + 32.0;
    print(":%f",temp);
    if (temp < 70.0)
      high(10);
     else
      low(10); 
}  

void autofeeder(void) { //might need to use a separate propellor
  //char x='0';
  //scanf("%c", &x);    //check data recieved
  //if(x == '1'){         // if recieved data is '1'
  //feedcounter = feedcounter + 1;  //increment counter if feeding
    servo_angle(16, 1100);  //servo instructions
    pause(1000);
    servo_angle(16, 0);
    pause(1000);
    servo_angle(16, 1100);
    pause(1000);
    servo_angle(16,0);
    pause(1000);
    servo_stop(); 
  //  print(":Feed Counter %d",feedcounter);
  //}else {
  //print(":Feed Counter %d",feedcounter); 
  //}  
}   

void alertsystem(void) {
 int right = input(15);
 if(right == 1) 
 {
   print(":Someone is Near");
   high(11);
   pause(200);
   low(11);
   pause(200);
   high(11);
   pause(200);
   low(11);
 }else{
  print(":Area Clear");
 }    
}   

void turtleactivity(void) {
 int activity = input(6);
 if(activity == 1)
 {
   print(":Turtle Movement");
 }else {
  print(":No Turtle Activity"); 
 }       
}   