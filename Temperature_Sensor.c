#include "simpletools.h"                      
#include "adcDCpropab.h"                      

int main()                                    
{
  adc_init(21, 20, 19, 18);                   

  float  v3, tempC, tempF;                              
  while(1)                                    
  {
                                       
    v3 = adc_volts(3);                        
    tempC = 31*v3;
    tempF = (tempC*1.8) + 32;
    putChar(HOME);                           
    
    print("Temperature = %f F%c", tempF, CLREOL);     

    pause(5000);                              
  }  
}