#include "simpletools.h"                      // Include simple tools
#include "string.h"
#include <stdio.h>
#include "servo.h"
  char str[] = "Happy";          //global variable
  int counterA = 1;
  int counterB = 1;
  int counterC = 1;
  char touch_out[] = "Default";
  
int main()                                    // Main function
{
  while(1){                //infinite loop for functions
    
    printString();
    touchsensor();
  }    
 }

void printString(){
   //While loop will have to be removed for final project!
          print("%s:%d:%d:%d",&str,counterA,counterB,counterC);
          ++counterB;
          ++counterC;
          ++counterC;
          pause(1000);            
   
}

void touchsensor(void){
 int touch = input(3);         //touch sensor set to pin 3
 if(touch == 1)                //if touch sensor is touched
 {                             //do motor instructions
     servo_angle(14, 0);
     pause(1000);
     servo_angle(14, 900);
     pause(1000);
     servo_angle(14, 0);
     servo_stop();
     char touch_out[] = "On";   //print sensor state
     print(":%s\n", &touch_out);
    }
  else                        //else print sensor state
  {
      char touch_out[] = "Off";
      print(":%s\n", &touch_out);  
    }      
}  
